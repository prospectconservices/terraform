terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.5.0"
    }
  }
}

terraform {
  backend "s3" {
    bucket         = "prospect-terraform-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "prospect-terraform-state-dynamodb"
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "environment" {
  description = "name of the environment"
  type = string
}

variable "vpc_cidr" {
  description = "cidr block of vpc"
  type = string
}

variable "avail_zone" {
  description = "availability zone of subnet"
  type = string
}

variable "subnet_cidr" {
  description = "Subnet 2 cidr block"
  type = string
}

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "Terraform-VPC",
    Env  = var.environment
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = var.avail_zone
  tags = {
    Name = "Terraform-Subnet-1",
    Env  = var.environment
  }
}

data "aws_vpc" "default_vpc" {
  default = true
}

resource "aws_subnet" "subnet_2" {
  vpc_id            = data.aws_vpc.default_vpc.id
  cidr_block        = var.subnet_cidr
  availability_zone = var.avail_zone
  tags = {
    Name = "Terraform-Subnet-2",
    Env  = var.environment
  }
}

output "vpc-id" {
  value = aws_vpc.vpc.id
}

output "subnet-1-az" {
  value = aws_subnet.subnet_1.availability_zone
}

output "subnet-2-cidr" {
  value = aws_subnet.subnet_2.cidr_block
}